import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './_guards';
import { OrderAdminComponent } from './_pages/order-admin/order-admin.component'

const appRoutes: Routes = [ 
    {path: '', component: OrderAdminComponent}
];
 
export const routing = RouterModule.forRoot(appRoutes);
