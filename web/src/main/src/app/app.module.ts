import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

//Import Helpers
import { JwtInterceptor, ErrorInterceptor } from './_helpers';

//Import guards
import { AuthGuard } from './_guards';

// Import Services
import { AlertService, AuthenticationService, UserService } from './_services';

// used to create fake backend
// import { fakeBackendProvider } from './_helpers';

// Import directives
import { AlertComponent } from './_directives';

//Import Componenets in the module
import { AppComponent } from './app.component';
import { LoginComponent } from './_pages/login';
import { HomeComponent } from './_pages/home';
import { RegisterComponent } from './_pages/register';
import { StatusFilterComponent } from './_components/status-filter/status-filter.component';
import { OrderAdminComponent } from './_pages/order-admin/order-admin.component'

//Import routing list
import { routing } from './app.routing';

@NgModule({
   declarations: [
      AppComponent,
      LoginComponent,
      HomeComponent,
      RegisterComponent,
      AlertComponent,
      StatusFilterComponent,
      OrderAdminComponent
   ],
   imports: [
      BrowserModule,
      ReactiveFormsModule,
      HttpClientModule,
      routing
   ],
   providers: [
    AuthGuard,
    AlertService,
    AuthenticationService,
    UserService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    // provider used to create fake backend
    // fakeBackendProvider
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
